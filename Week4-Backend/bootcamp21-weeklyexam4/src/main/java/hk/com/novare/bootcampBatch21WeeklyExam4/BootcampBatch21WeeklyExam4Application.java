package hk.com.novare.bootcampBatch21WeeklyExam4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootcampBatch21WeeklyExam4Application {

	public static void main(String[] args) {
		SpringApplication.run(BootcampBatch21WeeklyExam4Application.class, args);
	}

}
