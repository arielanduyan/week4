package hk.com.novare.bootcampBatch21WeeklyExam4.Controller;

import hk.com.novare.bootcampBatch21WeeklyExam4.Service.AccountService;
import hk.com.novare.bootcampBatch21WeeklyExam4.Service.UserService;
import hk.com.novare.bootcampBatch21WeeklyExam4.model.Account;
import hk.com.novare.bootcampBatch21WeeklyExam4.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/account")
public class AccountController {
    AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    //add account
    @PostMapping
    public ResponseEntity registerAccount(@RequestBody Account account){
        return ResponseEntity.ok(accountService.saveAccount(account));
    }

    //retrieve account
    @GetMapping
    public ResponseEntity getAllAccounts(){
        return ResponseEntity.ok(accountService.getAccounts());
    }

    //update user
    @PutMapping
    public ResponseEntity updateAccount(@RequestBody Account account, long id) {
        return ResponseEntity.ok(accountService.updateAccountsById(account, id));
    }

    //delete user
    @DeleteMapping
    public String deleteAccount(long id) {
        accountService.deleteAccountsById(id);

        return "User id: " + id + " Successfully Deleted";
    }
}

