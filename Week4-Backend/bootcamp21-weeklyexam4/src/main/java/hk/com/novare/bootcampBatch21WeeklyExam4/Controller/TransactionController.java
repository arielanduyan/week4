package hk.com.novare.bootcampBatch21WeeklyExam4.Controller;

import hk.com.novare.bootcampBatch21WeeklyExam4.Service.TransactionService;
import hk.com.novare.bootcampBatch21WeeklyExam4.model.Transaction;
import hk.com.novare.bootcampBatch21WeeklyExam4.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/transaction")
public class TransactionController {
    TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    //add account
    @PostMapping
    public ResponseEntity registerTransaction(@RequestBody Transaction transaction){
        return ResponseEntity.ok(transactionService.saveTransaction(transaction));
    }

    //retrieve account
    @GetMapping
    public ResponseEntity getAllTransaction(){
        return ResponseEntity.ok(transactionService.getTransaction());
    }

    @PutMapping
    public ResponseEntity updateTransaction(@RequestBody Transaction transaction, long id) {
        return ResponseEntity.ok(transactionService.updateTransactionById(transaction, id));
    }

    @DeleteMapping
    public String deleteUser(long id) {
        transactionService.deleteTransactionById(id);

        return "User id: " + id + " Successfully Deleted";
    }
}

