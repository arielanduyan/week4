package hk.com.novare.bootcampBatch21WeeklyExam4.Controller;

import hk.com.novare.bootcampBatch21WeeklyExam4.Repository.AccountRepository;
import hk.com.novare.bootcampBatch21WeeklyExam4.Repository.UserRepository;
import hk.com.novare.bootcampBatch21WeeklyExam4.Service.UserService;
//import hk.com.novare.bootcampBatch21WeeklyExam4.dto.JoinRequest;
import hk.com.novare.bootcampBatch21WeeklyExam4.model.Account;
import hk.com.novare.bootcampBatch21WeeklyExam4.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/users")
public class UserController {

    UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    //add single user
    @PostMapping(value = "/register")
    public ResponseEntity registerUser(@RequestBody User user){
        return ResponseEntity.ok(userService.saveUser(user));
    }

    //get all user
    @GetMapping
    public ResponseEntity getAllUsers(){
        return ResponseEntity.ok(userService.getUsers());
    }

    //update user
    @PutMapping
    public ResponseEntity updateUser(@RequestBody User user, long id) {
        return ResponseEntity.ok(userService.updateUsersById(user, id));
    }

    //delete user
    @DeleteMapping
    public String deleteUser(long id) {
        userService.deleteUsersById(id);

        return "User id: " + id + " Successfully Deleted";
    }

    /*Added By Ariel*/
    //validate unique username
    @GetMapping(value = "/register", params = {"username"})
    public ResponseEntity loginUser(@RequestParam String username){
        return ResponseEntity.ok(userService.validateUsername(username));
    }

    //Login User
    @GetMapping(value = "/login", params = {"username","password"})
    public ResponseEntity loginUser(@RequestParam String username, @RequestParam String password){
        return ResponseEntity.ok(userService.validateCredentials(username, password));
    }

    //Update Device Id and Status
    @PutMapping(value = "/update", params = {"deviceId","status","id"})
    public void updateUser(@RequestParam String deviceId, @RequestParam String status, @RequestParam long id) {
        userService.updateUserDetails(deviceId, status, id);
    }

    //Login Session
    @GetMapping(value = "/session", params = {"deviceId"})
    public ResponseEntity loginSession(@RequestParam String deviceId){
        return ResponseEntity.ok(userService.loginSession(deviceId));
    }

    //User Stats per Account
    @GetMapping(value = "/stats", params = {"id"})
    public List<List<String>> statsPerAccount(@RequestParam int id){
        return userService.statsPerAccount(id);
    }

    //User total stats
    @GetMapping(value = "/total", params = {"id"})
    public String totalStats(@RequestParam int id){
        return userService.totalStats(id);
    }

    //No.6
    @GetMapping(value = "/transactions/weekly", params = {"date", "id"})
    public ResponseEntity getAllTransactionsByDateWeek(@RequestParam String date, @RequestParam int id){
        return ResponseEntity.ok(userService.getAllTransactionByDateWeek(date, id));

    }

    @GetMapping(value = "/transactions/monthly", params = {"date", "id"})
    public ResponseEntity getAllTransactionsByDateMonth(@RequestParam String date, @RequestParam int id){
        return ResponseEntity.ok(userService.getAllTransactionByDateMonth(date, id));

    }

    //No.7
    @GetMapping(value = "/transactions", params = {"account", "id"})
    public ResponseEntity getAllTransactionsByAccount(@RequestParam String account, @RequestParam int id){
        return ResponseEntity.ok(userService.getAllTransactionByAccount(account, id));

    }

    //No.8
    @GetMapping(value = "/accounts", params = {"id"})
    public ResponseEntity getAllAccounts(@RequestParam int id){
        return ResponseEntity.ok(userService.getAllAccounts(id));

    }
}