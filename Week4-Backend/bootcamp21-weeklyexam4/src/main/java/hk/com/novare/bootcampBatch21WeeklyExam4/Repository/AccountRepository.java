package hk.com.novare.bootcampBatch21WeeklyExam4.Repository;

import hk.com.novare.bootcampBatch21WeeklyExam4.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
}
