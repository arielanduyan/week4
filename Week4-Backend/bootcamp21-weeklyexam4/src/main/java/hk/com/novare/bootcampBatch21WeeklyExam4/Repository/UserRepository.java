package hk.com.novare.bootcampBatch21WeeklyExam4.Repository;

import hk.com.novare.bootcampBatch21WeeklyExam4.model.User;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    //List<User> findAllByEmail();

    /*Added By Ariel*/
    @Query(value="SELECT username FROM user WHERE username = :username", nativeQuery = true)
    List<String> getUsername(@Param("username") String username);

    //login
    @Query(value="SELECT id, email, name, username, password, status, machine_id FROM user WHERE username = :username AND password = :password", nativeQuery = true)
    List<User> getCredentials(@Param("username") String username, @Param("password") String password);

    @Query(value="SELECT id, email, name, username, password, status, machine_id FROM user WHERE machine_id = :deviceId", nativeQuery = true)
    List<User> loginSession(@Param("deviceId") String deviceId);

    @Query(value="SELECT count(*) as total, account_name FROM transaction t INNER JOIN account a ON t.account_id = a.id WHERE a.user_id = :id GROUP BY a.account_name", nativeQuery = true)
    List<List<String>> stats(@Param("id") int id);

    @Query(value="SELECT count(*) as total FROM transaction t INNER JOIN account a ON t.account_id = a.id WHERE a.user_id = :id", nativeQuery = true)
    String totalStats(@Param("id") int id);

    //weekly
    @Query(value="SELECT t.bill, t.category, t.name, date_format(t.transaction_date, '%Y-%m-%d %h:%i %p') as tdate, date_format(:date, '%d %b %Y') as startDate, date_format((:date + INTERVAL 1 WEEK), '%d %b %Y') as endDate, a.account_name, a.user_id FROM transaction t INNER JOIN account a ON t.account_id = a.id WHERE t.transaction_date BETWEEN :date and (:date + INTERVAL 1 WEEK) AND a.user_id = :id ORDER BY t.transaction_date", nativeQuery = true)
    List<List<String>> getAllTransactionByDateWeek(@Param("date") String date, @Param("id") int id);

    //monthly
    @Query(value="SELECT t.bill, t.category, t.name, date_format(t.transaction_date, '%Y-%m-%d %h:%i %p') as tdate, date_format(:date, '%d %b %Y') as startDate, date_format((:date + INTERVAL 1 MONTH), '%d %b %Y') as endDate, a.account_name, a.user_id FROM transaction t INNER JOIN account a ON t.account_id = a.id WHERE t.transaction_date BETWEEN :date and (:date + INTERVAL 1 MONTH) AND a.user_id = :id ORDER BY t.transaction_date", nativeQuery = true)
    List<List<String>> getAllTransactionByDateMonth(@Param("date") String date, @Param("id") int id);

    @Query(value="SELECT a.account_name, a.user_id, t.bill, t.category, t.name, date_format(t.transaction_date, '%Y-%m-%d %h:%i %p') as tdate FROM account a INNER JOIN transaction t ON a.id = t.account_id WHERE a.account_name = :accountName AND a.user_id = :id ORDER BY t.transaction_date", nativeQuery = true)
    List<List<String>> getAllTransactionByAccount(@Param("accountName") String accountName, @Param("id") int id);

    @Query(value="SELECT a.user_id, a.account_name, a.balance, t.bill, t.name, date_format(t.transaction_date, '%Y-%m-%d %h:%i %p') as tdate FROM account a INNER JOIN transaction t ON a.id = t.account_id WHERE a.user_id= :id", nativeQuery = true)
    List<List<String>> getAllAccounts(@Param("id") int id);
}
