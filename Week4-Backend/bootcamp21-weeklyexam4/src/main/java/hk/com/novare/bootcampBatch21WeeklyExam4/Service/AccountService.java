package hk.com.novare.bootcampBatch21WeeklyExam4.Service;

import hk.com.novare.bootcampBatch21WeeklyExam4.model.Account;
import hk.com.novare.bootcampBatch21WeeklyExam4.model.User;

import java.util.List;

public interface AccountService {
    Account saveAccount(Account account);
    List<Account> getAccounts();
    Account updateAccountsById(Account account, long id);
    void deleteAccountsById(long id);
}
