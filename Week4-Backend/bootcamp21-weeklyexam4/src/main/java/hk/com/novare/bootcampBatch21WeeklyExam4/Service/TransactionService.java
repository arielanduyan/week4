package hk.com.novare.bootcampBatch21WeeklyExam4.Service;

import hk.com.novare.bootcampBatch21WeeklyExam4.model.Transaction;
import hk.com.novare.bootcampBatch21WeeklyExam4.model.User;

import java.util.List;

public interface TransactionService {
    Transaction saveTransaction(Transaction transaction);
    List<Transaction>getTransaction();
    Transaction updateTransactionById(Transaction transaction, long id);
    void deleteTransactionById(long id);
}
