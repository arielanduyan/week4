package hk.com.novare.bootcampBatch21WeeklyExam4.Service;

import hk.com.novare.bootcampBatch21WeeklyExam4.model.Account;
import hk.com.novare.bootcampBatch21WeeklyExam4.model.User;

import java.util.List;

public interface UserService {
    User saveUser(User user);
    List<User> getUsers();
    User updateUsersById(User user, long id);
    void deleteUsersById(long id);
    //List<User> getUserByEmail();

    /*Added By Ariel*/
    List<String> validateUsername(String username);
    List<User> validateCredentials(String username, String password);
    void updateUserDetails(String deviceId, String status, long id);
    List<User> loginSession(String deviceId);
    List<List<String>> statsPerAccount(int id);
    String totalStats(int id);

    //List<User> getAllTransactionByDate(String startDate, String endDate);
    List<List<String>> getAllTransactionByDateWeek(String date, int id);
    List<List<String>> getAllTransactionByDateMonth(String date, int id);
    List<List<String>> getAllTransactionByAccount(String accountName, int id);
    List<List<String>> getAllAccounts(int id);
}
