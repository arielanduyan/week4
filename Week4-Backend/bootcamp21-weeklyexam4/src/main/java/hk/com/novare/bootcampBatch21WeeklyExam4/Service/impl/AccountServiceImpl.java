package hk.com.novare.bootcampBatch21WeeklyExam4.Service.impl;

import hk.com.novare.bootcampBatch21WeeklyExam4.Repository.AccountRepository;
import hk.com.novare.bootcampBatch21WeeklyExam4.Repository.UserRepository;
import hk.com.novare.bootcampBatch21WeeklyExam4.Service.AccountService;
import hk.com.novare.bootcampBatch21WeeklyExam4.model.Account;
import hk.com.novare.bootcampBatch21WeeklyExam4.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {
    AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Account saveAccount(Account account){
        return accountRepository.save(account);
    }

    @Override
    public List<Account> getAccounts() {
        return accountRepository.findAll();
    }

    //PutMapping
    @Override
    public Account updateAccountsById(Account newAccounts, long id) {
        Account accounts = accountRepository.getOne(id);

        if (newAccounts.getAccountName() != null) {
            accounts.setAccountName(newAccounts.getAccountName());
        }
        if (newAccounts.getBalance() != 0) {
            accounts.setBalance(newAccounts.getBalance());
        }
        if (newAccounts.getExpenses() != 0) {
            accounts.setExpenses(newAccounts.getExpenses());
        }
        final Account updatedAccount = accountRepository.save(accounts);
        return updatedAccount;
    }

    //DeleteMapping
    @Override
    public void deleteAccountsById(long id) {
        accountRepository.deleteById(id);
    }
}
