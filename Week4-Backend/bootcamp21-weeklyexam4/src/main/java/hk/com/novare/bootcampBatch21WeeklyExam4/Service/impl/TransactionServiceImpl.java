package hk.com.novare.bootcampBatch21WeeklyExam4.Service.impl;

import hk.com.novare.bootcampBatch21WeeklyExam4.Repository.TransactionRepository;
import hk.com.novare.bootcampBatch21WeeklyExam4.Service.TransactionService;
import hk.com.novare.bootcampBatch21WeeklyExam4.model.Transaction;
import hk.com.novare.bootcampBatch21WeeklyExam4.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    TransactionRepository transactionRepository;
    public TransactionServiceImpl(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Override
    public Transaction saveTransaction(Transaction transaction) {
        return transactionRepository.save(transaction);
    }

    @Override
    public List<Transaction> getTransaction() {
        return transactionRepository.findAll();
    }

    @Override
    public Transaction updateTransactionById(Transaction newTransaction, long id) {
        Transaction transaction = transactionRepository.getOne(id);

        if (newTransaction.getName() != null) {
            transaction.setName(newTransaction.getName());
        }
        if (newTransaction.getCategory() != null) {
            transaction.setCategory(newTransaction.getCategory());
        }
        if (newTransaction.getTransactionDate() != null) {
            transaction.setTransactionDate(newTransaction.getTransactionDate());
        }
        if (newTransaction.getBill() != 0) {
            transaction.setBill(newTransaction.getBill());
        }
        final Transaction updatedTransaction = transactionRepository.save(transaction);
        return updatedTransaction;
    }

    //DeleteMapping
    @Override
    public void deleteTransactionById(long id) {
        transactionRepository.deleteById(id);
    }
}
