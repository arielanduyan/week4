package hk.com.novare.bootcampBatch21WeeklyExam4.Service.impl;

import hk.com.novare.bootcampBatch21WeeklyExam4.Repository.AccountRepository;
import hk.com.novare.bootcampBatch21WeeklyExam4.Repository.UserRepository;
import hk.com.novare.bootcampBatch21WeeklyExam4.Service.UserService;
import hk.com.novare.bootcampBatch21WeeklyExam4.model.Account;
import hk.com.novare.bootcampBatch21WeeklyExam4.model.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    //PutMapping
    @Override
    public User updateUsersById(User newUsers, long id) {
        User users = userRepository.getOne(id);

        if (newUsers.getName() != null) {
            users.setName(newUsers.getName());
        }
        if (newUsers.getUsername() != null) {
            users.setUsername(newUsers.getUsername());
        }
        if (newUsers.getPassword() != null) {
            users.setPassword(newUsers.getPassword());
        }
        if (newUsers.getEmail() != null) {
            users.setEmail(newUsers.getEmail());
        }
        final User updatedUser = userRepository.save(users);
        return updatedUser;
    }

    //DeleteMapping
    @Override
    public void deleteUsersById(long id) {
        userRepository.deleteById(id);
    }

//    @Override
//    public List<User> getUserByEmail(){
//        return userRepository.findAllByEmail();
//    }

    /*Added By Ariel*/
    @Override
    public List<String> validateUsername(String username) {
        return userRepository.getUsername(username);
    }

    @Override
    public List<User> validateCredentials(String username, String password) {
        return userRepository.getCredentials(username, password);
    }

    @Override
    public void updateUserDetails(String deviceId, String status, long id) {
        Optional<User> user = userRepository.findById(id);

        User newUser = user.get();
        newUser.setMachineId(deviceId);
        newUser.setStatus(status);
        userRepository.save(newUser);
    }

    @Override
    public List<User> loginSession(String deviceId) {
        return userRepository.loginSession(deviceId);
    }

    @Override
    public List<List<String>> statsPerAccount(int id) {
        return userRepository.stats(id);
    }

    @Override
    public String totalStats(int id) {
        return userRepository.totalStats(id);
    }

    @Override
    public List<List<String>> getAllTransactionByDateWeek(String date, int id) {
        return userRepository.getAllTransactionByDateWeek(date, id);
    }

    @Override
    public List<List<String>> getAllTransactionByDateMonth(String date, int id) {
        return userRepository.getAllTransactionByDateMonth(date, id);
    }

    @Override
    public List<List<String>> getAllTransactionByAccount(String accountName, int id) {
        return userRepository.getAllTransactionByAccount(accountName, id);
    }

    @Override
    public List<List<String>> getAllAccounts(int id) {
        return userRepository.getAllAccounts(id);
    }
}
