package hk.com.novare.bootcampBatch21WeeklyExam4.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    AuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        BCryptPasswordEncoder encoder = (BCryptPasswordEncoder) passwordEncoder();

        auth.inMemoryAuthentication()
                .withUser("admin")
                .password(encoder.encode("P@ssw0rd123"))
                .authorities("ROLE_ADMIN");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .authorizeRequests()

                .antMatchers(HttpMethod.GET,"/users").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.POST,"/users").permitAll()
                .antMatchers(HttpMethod.PUT,"/users").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.DELETE,"/users").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.GET,"/account").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.POST,"/account").permitAll()
                .antMatchers(HttpMethod.PUT,"/account").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.DELETE,"/account").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.GET,"/transaction").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.POST,"/transaction").permitAll()
                .antMatchers(HttpMethod.PUT,"/transaction").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.DELETE,"/transaction").hasAuthority("ROLE_ADMIN")

                .and()
                .httpBasic();

        http.httpBasic().authenticationEntryPoint(authenticationEntryPoint);
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
}
