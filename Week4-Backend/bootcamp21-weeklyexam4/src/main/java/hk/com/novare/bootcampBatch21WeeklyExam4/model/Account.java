package hk.com.novare.bootcampBatch21WeeklyExam4.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "account")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "account_name")
    String accountName;

    @Column(name = "balance")
    double balance;

    @Column(name = "expenses")
    double expenses;

    /*Comments By Ariel*/
//    @OneToMany(targetEntity = Transaction.class, cascade = CascadeType.ALL)
//    @JoinColumn(name="transaction_fk", referencedColumnName = "id", insertable = true)
//    private List<Transaction> transactions;

    /*Added By Ariel*/
    @ManyToOne
    @JoinColumn(name = "user_id", insertable = true)
    private User users;
}
