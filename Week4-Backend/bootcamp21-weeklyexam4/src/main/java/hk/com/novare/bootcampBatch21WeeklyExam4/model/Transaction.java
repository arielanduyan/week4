package hk.com.novare.bootcampBatch21WeeklyExam4.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "transaction")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;

    @Column(name = "name")
    String name;

    @Column(name = "category")
    String category;

    @Column(name = "transaction_date")
    @DateTimeFormat(pattern = "yyyy.MM.dd")
    Instant transactionDate;

    @Column(name = "bill")
    double bill;

    /*Added By Ariel*/
    @ManyToOne
    @JoinColumn(name = "account_id", insertable = true)
    private Account accounts;
}
