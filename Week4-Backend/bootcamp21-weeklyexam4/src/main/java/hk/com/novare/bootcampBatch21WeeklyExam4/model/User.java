package hk.com.novare.bootcampBatch21WeeklyExam4.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "user")
public class User {

    @Id
    //@TableGenerator(name = "EntityIdGen", table = "TABLE_GENERATOR", pkColumnValue = "Entity")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;

    @Column(name = "name")
    String name;

    @Column(name = "username")
    String username;

    @Column(name ="password")
    String password;

    @Column(name = "email")
    String email;

    /*Added By Ariel*/
    @Column(name = "status")
    String status;

    @Column(name = "machine_id")
    String machineId;

    /*Comments By Ariel*/
//    @OneToMany(targetEntity = Account.class, cascade = CascadeType.ALL)
//    @JoinColumn(name="account_fk", referencedColumnName = "id", insertable = true)
//    private List<Account> accounts;

}
