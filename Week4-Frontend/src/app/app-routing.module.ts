import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountComponent } from './account/account.component';
import { CashInComponent } from './account/cash-in/cash-in.component';
import { MakeTransactionComponent } from './account/make-transaction/make-transaction.component';
import { ManageAccountComponent } from './account/manage-account/manage-account.component';
import { AccountBalancesComponent } from './expense-tracker/account-balances/account-balances.component';
import { AccountTransactionComponent } from './expense-tracker/account-transaction/account-transaction.component';
import { ExpenseTrackerComponent } from './expense-tracker/expense-tracker.component';
import { MonthlyTransactionComponent } from './expense-tracker/monthly-transaction/monthly-transaction.component';
import { WeeklyTransactionComponent } from './expense-tracker/weekly-transaction/weekly-transaction.component';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { RegisterComponent } from './register/register.component';
import { SidebarComponent } from './sidebar/sidebar.component';

const routes: Routes = [
  {path: 'account', component: AccountComponent},
  {path: 'expense-tracker', component: ExpenseTrackerComponent},
  {path: 'cash-in', component: CashInComponent},
  {path: 'make-transaction', component: MakeTransactionComponent},
  {path: 'manage-account', component: ManageAccountComponent},
  {path: 'account-transaction', component: AccountTransactionComponent},
  {path: 'account-balances', component: AccountBalancesComponent},
  {path: 'weekly-transaction', component: WeeklyTransactionComponent},
  {path: 'monthly-transaction', component: MonthlyTransactionComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'dashboard', component: MainComponent},
  {path: 'dashboard', component: SidebarComponent},
  {path: '', redirectTo: "/login", pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
