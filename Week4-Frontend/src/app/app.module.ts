import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccountComponent } from './account/account.component';
import { ExpenseTrackerComponent } from './expense-tracker/expense-tracker.component';
import { CashInComponent } from './account/cash-in/cash-in.component';
import { ManageAccountComponent } from './account/manage-account/manage-account.component';
import { MakeTransactionComponent } from './account/make-transaction/make-transaction.component';
import { AccountTransactionComponent } from './expense-tracker/account-transaction/account-transaction.component';
import { AccountBalancesComponent } from './expense-tracker/account-balances/account-balances.component';
import { WeeklyTransactionComponent } from './expense-tracker/weekly-transaction/weekly-transaction.component';
import { MonthlyTransactionComponent } from './expense-tracker/monthly-transaction/monthly-transaction.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MainComponent } from './main/main.component';
import { HeaderComponent } from './header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    AccountComponent,
    ExpenseTrackerComponent,
    CashInComponent,
    ManageAccountComponent,
    MakeTransactionComponent,
    AccountTransactionComponent,
    AccountBalancesComponent,
    WeeklyTransactionComponent,
    MonthlyTransactionComponent,
    SidebarComponent,
    MainComponent,
    HeaderComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
