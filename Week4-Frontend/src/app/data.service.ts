import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { ThrowStmt } from '@angular/compiler';
import { map } from 'rxjs/operators';
import { User } from 'src/app/model/user';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private REST_API_SERVER_USERS = "http://localhost:8080/users";

  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;

  constructor(private httpClient: HttpClient, private route: Router) { 
    
  }

  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization' : 'Basic ' + btoa('admin:P@ssw0rd123')})};

  sendGetRequest(urlValue: String):Observable<any> {
    return this.httpClient.get(this.REST_API_SERVER_USERS + urlValue, this.httpOptions)
  }

  registerUsers(user: User) {
    return this.httpClient.post(this.REST_API_SERVER_USERS + "/register", user, this.httpOptions);
  }

  getUsername(username: String) {
    return this.httpClient.get(this.REST_API_SERVER_USERS + "/register?username=" + username, this.httpOptions)
  }

  login(username: any, password: any) {
    return this.httpClient.get(this.REST_API_SERVER_USERS + "/login?username=" + username + "&password=" + password , this.httpOptions);
  } 

  userDetails(username: string, id: string, deviceID: string, status: string) {
    return this.httpClient.put(this.REST_API_SERVER_USERS + "/update?deviceId=" + deviceID + "&status=" + status + "&id=" + id, this.httpOptions);
  }

  loginSession(deviceID: string) {
    return this.httpClient.get(this.REST_API_SERVER_USERS + "/session?deviceId=" + deviceID , this.httpOptions);
  } 

  stats(id: string) {
    return this.httpClient.get(this.REST_API_SERVER_USERS + "/stats?id=" + id , this.httpOptions);
  }

  totalStats(id: string) {
    return this.httpClient.get(this.REST_API_SERVER_USERS + "/total?id=" + id , this.httpOptions);
  }
  
}
