import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-account-balances',
  templateUrl: './account-balances.component.html',
  styleUrls: ['./account-balances.component.scss']
})
export class AccountBalancesComponent implements OnInit {

  account: any = [];
  wallet: any = [];
  savings: any = [];
  credit: any = [];
  str: string;
  url: string = "/accounts?id=";
  dataFound = false;
  walletBalance: any;
  savingBalance: any;
  creditBalance: any;
  totalWalletExpense: any = 0;
  totalSavingExpense: any = 0;
  totalCreditExpense: any = 0;
  grandBalance: any;
  grandExpense: any;

 constructor(private dataService: DataService) {}

  ngOnInit(): void {

    this.account.splice(0, this.account.length);
    var userid: any = localStorage.getItem('userid');
    this.dataService.sendGetRequest(this.url + userid).subscribe((accounts = []) => {

      for (var i in accounts){
        this.account.push({
            uid: accounts[i][0],
            account: accounts[i][1],
            balance: accounts[i][2],
            expenses: accounts[i][3],
            transaction_name: accounts[i][4],
            transaction_date: accounts[i][5]
        });
      }

      for (var j in this.account){
        if (this.account[j]['account'] == 'Wallet'){
          this.wallet.push({
            balance: this.account[j]['balance'],
            expenses: this.account[j]['expenses'],
            transaction_name: this.account[j]['transaction_name'],
            transaction_date: this.account[j]['transaction_date']
          });
          this.totalWalletExpense += parseFloat(this.account[j]['expenses']);
          this.walletBalance = (this.account[0]['balance'] - this.totalWalletExpense);
        } else if (this.account[j]['account'] == 'Savings') {
          this.savings.push({
            balance: this.account[j]['balance'],
            expenses: this.account[j]['expenses'],
            transaction_name: this.account[j]['transaction_name'],
            transaction_date: this.account[j]['transaction_date']
          });
          this.totalSavingExpense += parseFloat(this.account[j]['expenses']);
          this.savingBalance = (this.account[0]['balance'] - this.totalSavingExpense);
        } else {
          this.credit.push({
            balance: this.account[j]['balance'],
            expenses: this.account[j]['expenses'],
            transaction_name: this.account[j]['transaction_name'],
            transaction_date: this.account[j]['transaction_date']
          });
          this.totalCreditExpense += parseFloat(this.account[j]['expenses']);
          this.creditBalance = (this.account[0]['balance'] - this.totalCreditExpense);
        }
        this.grandExpense = this.totalCreditExpense + this.totalSavingExpense + this.totalWalletExpense;
        this.grandBalance = (parseFloat(this.creditBalance) + parseFloat(this.savingBalance) + parseFloat(this.walletBalance));
      }
    })
  }
}


   

