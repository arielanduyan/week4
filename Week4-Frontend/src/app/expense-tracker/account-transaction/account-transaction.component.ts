import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-account-transaction',
  templateUrl: './account-transaction.component.html',
  styleUrls: ['./account-transaction.component.scss']
})

export class AccountTransactionComponent implements OnInit {

  account: any = [];
  str: string;
  url: string = "/transactions?account=";
  dataFound = false;
  accountName: string;

 constructor(private dataService: DataService) {}

  ngOnInit(): void {}

  refreshRow() {
    this.account.splice(0, this.account.length);
  }

  onEnter(dateValue: String) {

    
    this.refreshRow();
    var userid: any = localStorage.getItem('userid');
    this.dataService.sendGetRequest(this.url + dateValue + "&&id=" + userid).subscribe((accounts = []) => {

      this.accountName = accounts[0][0];

      for (var i in accounts){
        this.account.push({
            uid: accounts[i][1],
            bill: accounts[i][2],
            category: accounts[i][3],
            transaction_name: accounts[i][4],
            transaction_date: accounts[i][5]
        });
      }
    })
  }
}


