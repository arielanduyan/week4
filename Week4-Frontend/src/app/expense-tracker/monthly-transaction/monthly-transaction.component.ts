import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-monthly-transaction',
  templateUrl: './monthly-transaction.component.html',
  styleUrls: ['./monthly-transaction.component.scss']
})

export class MonthlyTransactionComponent implements OnInit {

  monthly: any = [];
  str: string;
  url: string = "/transactions/monthly?date=";
  dataFound = false;
  startDate: string;
  endDate: string;

  constructor(private dataService: DataService) {}

  ngOnInit(): void {}

  refreshRow() {
    this.monthly.splice(0, this.monthly.length);
  }

  onEnter(dateValue: String) {

    this.refreshRow();
    var userid: any = localStorage.getItem('userid');
    this.dataService.sendGetRequest(this.url + dateValue + "&&id=" + userid).subscribe((months = []) => {

      this.startDate = months[0][4];
      this.endDate = months[0][5];
      for (var i in months){
        this.monthly.push({
            bill: months[i][0],
            category: months[i][1],
            transaction_name: months[i][2],
            transaction_date: months[i][3],
            startDate: months[i][4],
            endDate: months[i][5],
            account_name: months[i][6],
            uid: months[i][7]
        });
      }
    })
  }
}
