import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeeklyTransactionComponent } from './weekly-transaction.component';

describe('WeeklyTransactionComponent', () => {
  let component: WeeklyTransactionComponent;
  let fixture: ComponentFixture<WeeklyTransactionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WeeklyTransactionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeeklyTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
