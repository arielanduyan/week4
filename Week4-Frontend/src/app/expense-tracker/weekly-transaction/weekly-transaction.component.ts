import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-weekly-transaction',
  templateUrl: './weekly-transaction.component.html',
  styleUrls: ['./weekly-transaction.component.scss']
})

export class WeeklyTransactionComponent implements OnInit {

   weekly: any = [];
   str: string;
   url: string = "/transactions/weekly?date=";
   dataFound = false;
   startDate: string;
   endDate: string;

  constructor(private dataService: DataService) {}

  ngOnInit(): void {}

  refreshRow() {
    this.weekly.splice(0, this.weekly.length);
  }

  onEnter(dateValue: String) {

    this.refreshRow();
    var userid: any = localStorage.getItem('userid');
    this.dataService.sendGetRequest(this.url + dateValue + "&&id=" + userid).subscribe((weeks = []) => {

      this.startDate = weeks[0][4];
      this.endDate = weeks[0][5];
      for (var i in weeks){
        this.weekly.push({
            bill: weeks[i][0],
            category: weeks[i][1],
            transaction_name: weeks[i][2],
            transaction_date: weeks[i][3],
            startDate: weeks[i][4],
            endDate: weeks[i][5],
            account_name: weeks[i][6],
            uid: weeks[i][7]
        });
      }
    })
  }
}

