import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { DataService } from 'src/app/data.service';
import { DeviceUUID } from 'device-uuid';
import { SidebarComponent } from 'src/app/sidebar/sidebar.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [SidebarComponent]
})
export class LoginComponent implements OnInit {
  
  form: FormGroup;
  loading = false;
  submitted = false;
  notMatched = false;
  returnUrl: string;
  userId: string;
  userName: string;
  sessionStatus: string;
  uuid = new DeviceUUID().get();

  loginForm = this.formBuilder.group({
    username: '',
    password: ''
  });

  constructor(
    private router: Router,
    private dataService: DataService,
    private formBuilder: FormBuilder,
    private sideBar: SidebarComponent
  ) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.dataService.loginSession(this.uuid).subscribe((session: any) => {
      if (session.length == 0) {
        this.sideBar.isOut = true;
        
      } else {
        this.sessionStatus = session[0]['status'];
        if (this.sessionStatus == 'active') {
          this.router.navigate(['/dashboard']);
          
        } else {
          this.sideBar.isOut = true;
          
        }
      }
    });
  }
  
  // convenience getter for easy access to form fields
  get f() { 
    return this.loginForm.controls; 
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }

    this.loading = true;
    this.dataService.login(this.f.username.value, this.f.password.value).pipe(first()).subscribe((data: any) => {

      if (data.length == 0) {
                
        let loginTimer1 = setInterval(() => {  
          this.loading = false;
        }, 2000);

        this.notMatched = true;
        // this.f.username.setErrors({notMatched: true});
        // this.f.password.setErrors({notMatched: true});

      } else {
                
        let loginTimer2 = setInterval(() => {  
          this.loading = false;
        }, 2000);

        this.userId = data[0]['id'];
        this.userName = data[0]['name']

        //patchMapping for status and machineId
        this.dataService.userDetails(this.userName, this.userId, this.uuid, 'active').subscribe(u =>{});
                
        this.router.navigate(['/dashboard']);
        this.loginForm.reset();
        
        localStorage.setItem('userid',data[0]['id']);
        localStorage.setItem('username',data[0]['name']);
      }  
    },
    error => {
      this.loading = false;
    });
  }
}
