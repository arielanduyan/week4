import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  userName: any;
  userID: any;
  totalStats: any;
  totalWallet: any;
  totalSaving: any;
  totalCredit: any;
  walletPercentage: any;
  savingPercentage: any;
  creditPercentage: any;
  statsData: any = [];
  sessionStatus: string;

  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit(): void {

    this.userName = localStorage.getItem('username');
    this.userID = localStorage.getItem('userid');
    this.dataService.stats(this.userID).subscribe((accStats: any = []) => {

      for (var i in accStats){
        this.statsData.push({
            totalCount: accStats[i][0],
            account: accStats[i][1]
        });
      }

      this.dataService.totalStats(this.userID).subscribe(total => {
        
        for (var j in this.statsData){
          if (this.statsData[j]['account'] == 'Wallet'){
            this.totalWallet = this.statsData[j]['totalCount'];
            
          } else if (this.statsData[j]['account'] == 'Savings'){
            this.totalSaving = this.statsData[j]['totalCount'];
            
          } else {
            this.totalCredit = this.statsData[j]['totalCount'];
            
          }
        }

        if (total == '0') {
          this.walletPercentage = '0%';
          this.savingPercentage = '0%';
          this.creditPercentage = '0%';
        } else {

          this.totalStats = total;
          this.walletPercentage = Math.round((this.totalWallet/this.totalStats) * 100);
          this.savingPercentage = Math.round((this.totalSaving/this.totalStats) * 100);
          this.creditPercentage = Math.round((this.totalCredit/this.totalStats) * 100);

          this.walletPercentage = this.walletPercentage + '%';
          this.savingPercentage = this.savingPercentage + '%';
          this.creditPercentage = this.creditPercentage + '%';

        }
        
      });
    });
  }

}
