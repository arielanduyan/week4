export class User {
    email: string;
    name: string;
    password: string;
    username: string;
}