export class Weekly {
    bill: string;
    category: string;
    transaction_name: string;
    transaction_date: string;
    startDate: string;
    endDate: string;
    account_name: string;
    uid: string;
  }