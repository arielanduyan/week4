import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;
  loading = false;
  submitted = false;
  notUnique = false;
  checked = false;

  registerForm = this.formBuilder.group({
    name: '',
    email: '',
    username: '',
    password: ''
  });

  constructor(
        private router: Router,
        private dataService: DataService,
        private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {

    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required, Validators.pattern],
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8)]]
  });

  }
  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    if (this.f.name.errors || this.f.email.errors || this.f.username.errors || this.f.password.errors){
          
    } else {

      //validate unique username
      this.dataService.getUsername(this.f.username.value).subscribe((uname: any = []) => {
        if (uname.length > 0) {

          this.notUnique = true;
          this.f.username.setErrors({notUnique: true})

        } else {

          this.loading = true;
          this.dataService.registerUsers(this.registerForm.value).pipe(first()).subscribe(data => {
                
            let registerTimer1 = setInterval(() => {  
              this.loading = false;
              this.checked = true;

              let registerTimer2 = setInterval(() => {  
                this.registerForm.reset();
                this.router.navigate(["login"]);
                clearInterval(registerTimer1);
                clearInterval(registerTimer2);
              }, 3000);

            }, 1000);

          }, error => {
            this.loading = false;
          });
        }
      });
    }
  }
}


