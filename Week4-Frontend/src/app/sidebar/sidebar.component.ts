import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
//import { timer } from 'bootcamp21-weeklyexam4-website/node_modules/rxjs';
import { interval } from 'rxjs';
import { map } from 'rxjs/operators';
//import { LoginComponent } from 'src/app/login/login.component';
import { DataService } from 'src/app/data.service';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit{

  @ViewChild('show') shows: ElementRef<any>;

  isLoading = false;
  isOut = false;
  time = 10;
  
  @Input() userName: string;

  constructor(private router: Router, private dataService: DataService) {}

  ngOnInit(): void {
    
  }
  
  logout(){
    this.isLoading = true;
    let logoutTimer = setInterval(() => {  
      this.isLoading = false;
      //this.isOut = true;
      var userid: any = localStorage.getItem('userid')
      this.dataService.userDetails('', userid, '', 'inactive').subscribe(out =>{});
      localStorage.clear();
      this.router.navigate(["login"]);
      clearInterval(logoutTimer);
    }, 3000);

  }
}
